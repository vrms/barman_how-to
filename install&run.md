
## configure PostgreSQL server

https://www.postgresql.org/download/linux/redhat/

Install the repository RPM:
```
sudo dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm
```
Disable the built-in PostgreSQL module: `sudo dnf -qy module disable postgresql`

Install barman: `sudo dnf install -y barman-cli`


```
sudo dnf update
sudo dnf install barman-cli
```

...  
...  
...  

postgres@postgres:> psql -d [mydb]
```
[mydb] # Show max_wal_senders;
 max_wal_senders 
-----------------
 10
(1 row)
```
```
[mydb]# Show max_replication_slots;
 max_replication_slots 

 10
(1 row)
```

```
[mydb]# select * from actor where last_name='KILMER';
\q
```

## configure barman backup-server



https://www.postgresql.org/download/linux/redhat/

Install the repository RPM:
```
sudo dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm
```
Disable the built-in PostgreSQL module: `sudo dnf -qy module disable postgresql`

Install barman: `sudo dnf install -y barman`


## Configuration

```
cat <<'EOF' >> /etc/barman.d/pg.conf
[pg]
description =  "Example of PostgreSQL Database (Streaming-Only)"
conninfo = host=pg user=barman dbname=pagila
streaming_conninfo = host=pg user=streaming_barman dbname=pagila
backup_method = postgres
streaming_archiver = on
slot_name = barman
create_slot = auto
EOF
```

become the `barman` user

```
sudo@barman:$ su - barman
```

create a .pgpass file for the postgres-server

```
cat <<'EOF' >>~/.pgpass
pg:*:*:barman:example-password
pg:*:*:streaming_barman:example-password
EOF
chmod 0600 ~/.pgpass
```



Verifying the configuration
```
barman@backup:> barman cron
```


```
barman@backup:~$ barman cron
Starting WAL archiving for server pg
```

check the configuration
```
barman@backup:~$ barman check pg
Server pg:
        WAL archive: FAILED (please make sure WAL shipping is setup)
        PostgreSQL: OK
        superuser or standard user with backup privileges: OK
        PostgreSQL streaming: OK
        wal_level: OK
        replication slot: OK
        directories: OK
        retention policy settings: OK
        backup maximum age: OK (no last_backup_maximum_age provided)
        backup minimum size: OK (0 B)
        wal maximum age: OK (no last_wal_maximum_age provided)
        wal size: OK (0 B)
        compression settings: OK
        failed backups: OK (there are 0 failed backups)
        minimum redundancy requirements: OK (have 0 backups, expected at least 0)
        pg_basebackup: OK
        pg_basebackup compatible: OK
        pg_basebackup supports tablespaces mapping: OK
        systemid coherence: OK (no system Id stored on disk)
        pg_receivexlog: OK
        pg_receivexlog compatible: OK
        receive-wal running: OK
        archiver errors: OK
```



```
barman switch-wal --archive --archive-timeout 60 pg

Output

The WAL file 000000010000000000000002 has been closed on server 'pg'
Waiting for the WAL file 000000010000000000000002 from server 'pg' (max: 60 seconds)
Processing xlog segments from streaming for pg
        000000010000000000000002
```
This forces WAL rotation and (with the --archive option) waits for the WAL to arrive. We'll give it 60 seconds (with the --archive-timeout option) to complete; if it doesn't complete within that amount of time, try again. For more detail on these commands and their options, refer to the Barman manual.


run the check again and you should see all checks passing
```
barman@backup:~$ barman check pg
Server pg:
        PostgreSQL: OK
        superuser or standard user with backup privileges: OK
        PostgreSQL streaming: OK
        wal_level: OK
        replication slot: OK
        directories: OK
        retention policy settings: OK
        backup maximum age: OK (no last_backup_maximum_age provided)
        backup minimum size: OK (0 B)
        wal maximum age: OK (no last_wal_maximum_age provided)
        wal size: OK (0 B)
        compression settings: OK
        failed backups: OK (there are 0 failed backups)
        minimum redundancy requirements: OK (have 0 backups, expected at least 0)
        pg_basebackup: OK
        pg_basebackup compatible: OK
        pg_basebackup supports tablespaces mapping: OK
        systemid coherence: OK (no system Id stored on disk)
        pg_receivexlog: OK
        pg_receivexlog compatible: OK
        receive-wal running: OK
        archiver errors: OK
        ```
## run a backup

### on the barman backup server
```
barman@backup:~$ barman backup pg
