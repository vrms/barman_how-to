based on


### postgresql settings
```
[postgres@barman-2 ~]$ psql -c "select name,setting,unit from pg_settings where name in ('wal_level','max_wal_senders','max_replication_slots');"
          name          | setting | unit 
 -----------------------+---------+------
  max_replication_slots | 10      | 
  max_wal_senders       | 10      | 
  wal_level             | replica | 

```


### barman postgresql user
```
[postgres@barman-2 ~]$ psql -c "CREATE USER barman SUPERUSER password 'barman';" 
[postgres@barman-2 ~]$ psql -c "CREATE USER streaming_barman WITH replication  password 'barman';"
```


```
 [postgres@barman-2 ~]$ psql -U streaming_barman -h localhost -c "IDENTIFY_SYSTEM" replication=1
```

### barman.conf
```
root@barman-2 etc]# vi barman.conf
[barman]
barman_user = barman
configuration_files_directory = /etc/barman.d
barman_home = /var/lib/barman`
log_file = /var/log/barman/barman.log
log_level = INFO
compression = gzip
immediate_checkpoint = true
basebackup_retry_times = 3
minimum_redundancy = 3
retention_policy = RECOVERY WINDOW OF 7 DAYS
```


### barman server config
```
root@barman-2 barman.d]# vi postgres-local.conf
[postgres-local]
description =  "Example of PostgreSQL Database (Streaming-Only)"
conninfo = host=test-machine02 user=barman port=5432
streaming_conninfo = host=test-machine02 user=streaming_barman port=5432
backup_method = postgres
streaming_archiver = on
slot_name = barman
create_slot = auto
:wq!
```

### .pgpass file (barman user)
```
[barman@barman-2 ~]$ vi .pgpass
localhost:5432:postgres:barman:barman
localhost:5432:replication:streaming_barman:barman
[barman@barman-2 ~]$ chmod 0600 .pgpass
```
