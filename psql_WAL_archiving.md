### in `postgresql.conf`

- `wal_level` to `replica` or higher (to-do: understand all levels)
- `archive_mode` = `on`
- `archive_command = 'test ! -f /mnt/server/archivedir/%f && cp %p /mnt/server/archivedir/%f'`
     - `%f` is replaced by the file name of the WAL file automatically
     - `%p` is replaced by the path name of the file to archive
     - Use `'%%'` if you need to embed an actual `'%'` character in the command
     - --> `test ! -f /mnt/server/archivedir/00000001000000A900000065 && cp pg_wal/00000001000000A900000065 /mnt/server/archivedir/00000001000000A900000065`
- make sure to archive into a directory that does not have group or world read access